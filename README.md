## Ziel
Entwicklung einer Single-Page-Angular-Webanwendung zum erstellen von Inhaltsverzeichnissen und Trennblättern auf mehrerern tiefen. Diese können nach Erstellung einer Druckansicht und druck in Physikalischen Ordnern/Heftern verwendet werden.

## Soll-Zustand
*  Über die Oberfläche sollen beliebig viele Ordner verwaltet werden können
*  In jedem Ordner sollen Überschiften auf drei Ebenen erstellt werden
*  Export
    * Json Export soll extra Ablage auserhalb des Browsers ermöglichen
    * Nach der Fertigstellung eines Ordners sollen Benuter den Ordner als PDF heunterladen können 
* Die Ordner sollen zunächst als Cookie gespeichert werden

## Future Features
*  Schaffen einer Möglichkeit erstellte Ordner zu Teilen und als Vorlagen bereit zustellen
*  Möglichkeit Theme für die Druckansicht anzupassen
    * Farben
    * Schiftart
    * Logos (Coperate Identity)
*  Multilingual

## Mockups

Hinweise zu Den Mockups:
*  Graue Werte dienen nur als Platzhalter
*  Farben, Schirftarten und Anordnung sind nur exemplatisch

### Desktop Mockup
![Desktop Mockup](doku/Mockup_Main_View.jpg "Desktop Mockup")

### Mobile Mockup
(Bilschirmbreite < 1000px)


<img src="doku/Mockup_Main_View_Mobile.jpg"  width="600">

